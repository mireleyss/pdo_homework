<?php

    include_once "connection_data.php";
    include_once "classes/database.php";
    include_once "classes/user.php";

    $db = new Database($dbhost, $dbname, $dbuser, $dbpass);
    $conn = $db->connect();

    $user = new User('Jack', 'jack@gmail.com', 23, 'Netherlands', $conn);

    $id = $user->create();

    $field = 'age';
    $updateValue = 19;

    $user->update($field, $id, $updateValue);

    print_r($user->read($id));

    $user->delete($id);