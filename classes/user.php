<?php

    class User {
        private $name;
        private $email;
        private $age;
        private $country;
        private $conn;
        private $dbTable = 'users';

        public function __construct($name, $email, $age, $country, $conn)
        {
            $this->name = $name;
            $this->email = $email;
            $this->age = $age;
            $this->country = $country;
            $this->conn = $conn;
        }

        public function create() {
            $query = "INSERT INTO $this->dbTable (name, email, age, country) VALUES (:name, :email, :age, :country)";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':name', $this->name);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':age', $this->age);
            $stmt->bindParam(':country', $this->country);

            $stmt->execute();

            return $this->conn->lastInsertId();
        }

        public function update($field, $id, $updateValue) {
            $query = "UPDATE $this->dbTable SET $field = :value WHERE id = :id";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':value', $updateValue);
            $stmt->bindParam(':id', $id);

            $stmt->execute();
        }

        public function read($id) {
            $query = "SELECT * FROM $this->dbTable WHERE id = :id";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $id);

            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }

        public function delete($id) {
            $query = "DELETE FROM $this->dbTable WHERE id = :id";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $id);

            $stmt->execute();
        }

    }