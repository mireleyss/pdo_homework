<?php
    class Database {

        private $dbhost;
        private $dbname;
        private $dbuser;
        private $dbpass;
        private $conn;

        public function __construct($dbhost, $dbname, $dbuser, $dbpass)
        {
            $this->dbhost = $dbhost;
            $this->dbname = $dbname;
            $this->dbuser = $dbuser;
            $this->dbpass = $dbpass;
        }

        function connect() {
            try {
                $this->conn = $pdo = new PDO("mysql:host=$this->dbhost; dbname=$this->dbname", $this->dbuser, $this->dbpass);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                return $this->conn;

            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
                exit();
            }
        }
    }